package edu.ucsd.cs11w.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(CelsiusTests.class);
		suite.addTestSuite(FahrenheitTests.class);
		//$JUnit-END$
		return suite;
	}

}
