/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author dmariano
 *
 */
public class Fahrenheit extends Temperature{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
	// TODO: Complete this method
	return getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		float temp = getValue();
		temp -= 32;
		System.out.println(temp);
		temp = temp * 5 / 9;
		System.out.println(temp);
		Celsius ans = new Celsius(temp);
		// TODO Auto-generated method stub
		return ans;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
}
