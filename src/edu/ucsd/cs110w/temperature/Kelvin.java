/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (dmariano): write class javadoc
 *
 * @author dmariano
 *
 */
public class Kelvin extends Temperature{
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		// TODO: Complete this method
		float temp = getValue();
		temp -= 273.15;
		Celsius ans = new Celsius(temp);		
		return ans;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO: Complete this method
		Temperature cel = this.toCelsius();
		Temperature ans = cel.toFahrenheit();
		return ans;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		return this;
	}
}
