/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author dmariano
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	} 
	public String toString()
	{
		// TODO: Complete this method 
		return getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float temp = getValue();
		temp = temp / 5 * 9 ;
		temp += 32;
		Fahrenheit ans = new Fahrenheit(temp);		
		return ans;
	}
}
